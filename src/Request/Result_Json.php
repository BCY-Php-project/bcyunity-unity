<?php

namespace Bcyunity\Unity\Request;



class Result_Json
{
    /**
     * 返回Json数据
     * @param int $code Json格式的返回代码
     * @param string $message 提示信息
     * @param array $data 数组型
     * @return string 
     */
    public static function return_Json($code,$message = '',$data = array()){
        //  判断$code是否为数字
        if(!is_numeric($code)){
            $code = 400;
        }
        switch ($code) {
            case '200':
                $message = 'successfully';
                break;

            default:
                $message = 'sunsuccessful';
                break;
        }
        
        $result = array(
            'code' => $code,
            'message' => $message,
            'data' => $data
        );
        // 若不添加第二个参数将会返回一个URL编码后的结果，致使中文仍要二次url解码才能显示
        return $result;
        
    }


    
}
